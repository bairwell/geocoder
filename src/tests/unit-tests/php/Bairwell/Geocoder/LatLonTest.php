<?php
namespace Bairwell\Geocoder;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2012-07-14 at 16:45:53.
 */
class LatLonTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var LatLon
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new LatLon;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }


    /**
     * @covers Bairwell\Geocoder\LatLon::setLat
     * @covers Bairwell\Geocoder\LatLon::getLat
     */
    public function testLat()
    {
        $this->assertEquals(0,$this->object->getLat());
        $this->object->setLat(51.467966);
        $this->assertEquals(51.467966,$this->object->getLat(),'',0.000001);
    }

    /**
     * @covers Bairwell\Geocoder\LatLon::setLon
     * @covers Bairwell\Geocoder\LatLon::getLon
     */
    public function testLon()
    {
        $this->assertEquals(0,$this->object->getLon());
        $this->object->setLon(-0.017770);
        $this->assertEquals(-0.017770,$this->object->getLon(),'',0.0000001);
    }

    /**
     * @covers Bairwell\Geocoder\LatLon::setRadius
     * @covers Bairwell\Geocoder\LatLon::getRadius
     */
    public function testRadius()
    {

        $this->assertEquals(6371,$this->object->getRadius());
        $this->object->setRadius(4643.32);
        $this->assertEquals(4643.32,$this->object->getRadius(),'',0.01);
    }


    /**
     * @covers Bairwell\Geocoder\LatLon::distanceTo
     * @depends testLon
     * @depends testLat
     */
    public function testDistanceTo()
    {
        $this->object->setLat(51.4679656497414);
        $this->object->setLon(-0.0177698697591059);
        $new=new LatLon( 51.142510,0.840449);
        $this->assertEquals(69.78,$this->object->distanceTo($new,4),'');
    }

    /**
     * @covers Bairwell\Geocoder\LatLon::bearingTo
     * @depends testLon
     * @depends testLat
     */
    public function testBearingTo()
    {
        $this->object->setLat(51.4679656497414);
        $this->object->setLon(-0.0177698697591059);
        $new=new LatLon( 51.142510,0.840449);
        $this->assertEquals(120.9051,$this->object->bearingTo($new),'',4);
    }

    /**
     * @covers Bairwell\Geocoder\LatLon::finalBearingTo
     * @depends testLon
     * @depends testLat
     */
    public function testFinalBearingTo()
    {
        $this->object->setLat(51.4679656497414);
        $this->object->setLon(-0.0177698697591059);
        $new=new LatLon( 51.142510,0.840449);
        $this->assertEquals(121.5750,$this->object->finalBearingTo($new),'',4);
    }

    /**
     * @covers Bairwell\Geocoder\LatLon::midpointTo
     * @depends testLon
     * @depends testLat
     */
    public function testMidpointTo()
    {
        $this->object->setLat(51.4679656497414);
        $this->object->setLon(-0.0177698697591059);
        $new=new LatLon( 51.142510,0.840449);
        $result=$this->object->midPointTo($new);
        $this->assertTrue($result instanceof LatLon,'Return type');
        $this->assertEquals(51.3060,$result->getLat(),'',4);
        $this->assertEquals(0.4129,$result->getLon(),'',4);
    }

    /**
     * @covers Bairwell\Geocoder\LatLon::destinationPoint
     * @depends testLon
     * @depends testLat
     */
    public function testDestinationPoint()
    {
        $this->object->setLat(51.4679656497414);
        $this->object->setLon(-0.0177698697591059);
        $result=$this->object->destinationPoint(47.6349,57.23);
        $this->assertTrue($result instanceof LatLon,'Return type');
        $this->assertEquals(51.81298741,$result->getLat(),'',8);
        $this->assertEquals( 0.59538543,$result->getLon(),'',8);
    }

    /**
     * @covers Bairwell\Geocoder\LatLon::intersection
     * @depends testLon
     * @depends testLat
     */
    public function testIntersection()
    {
        $p1=new LatLon(51.467965,-0.017769);
        $p2=new LatLon(51.142510, 0.840449);
        $result=$this->object->intersection($p1, 108.63, $p2, 32.72);
        $this->assertTrue($result instanceof LatLon,'Return type');
        $this->assertEquals(51.2578,$result->getLat(),'',4);
        $this->assertEquals( 0.9589,$result->getLon(),'',4);
        $result=$this->object->intersection($p2, 108.63, $p1, 32.72);
        $this->assertTrue($result instanceof LatLon,'Return type');
        $this->assertEquals(-51.3456,$result->getLat(),'',4);
        $this->assertEquals(179.8564,$result->getLon(),'',4);

    }

    /**
     * @covers Bairwell\Geocoder\LatLon::rhumbDistanceTo
     * @depends testLon
     * @depends testLat
     */
    public function testRhumbDistanceTo()
    {
        $this->object->setLat(50.3639);
        $this->object->setLon(-4.1569);
        $new=new LatLon(42.3511, -71.0408);
        $result=$this->object->rhumbDistanceTo($new);
        $this->assertEquals(5196,$result);
    }

    /**
     * @covers Bairwell\Geocoder\LatLon::rhumbBearingTo
     * @depends testLon
     * @depends testLat
     */
    public function testRhumbBearingTo()
    {
        $this->object->setLat(50.3639);
        $this->object->setLon(-4.1569);
        $new=new LatLon(42.3511, -71.0408);
        $result=$this->object->rhumbBearingTo($new);
        $this->assertEquals(260.1271,$result,'',4);
    }

    /**
     * @covers Bairwell\Geocoder\LatLon::rhumbDestinationPoint
     * @depends testLon
     * @depends testLat
     */
    public function testRhumbDestinationPoint()
    {
        $this->object->setLat(50.3639);
        $this->object->setLon(-4.1569);
        $result=$this->object->rhumbDestinationPoint(116.63,40.23);
        $this->assertTrue($result instanceof LatLon,'Return type');
        $this->assertEquals(50.20170,$result->getLat(),'',5);
        $this->assertEquals(-3.65080,$result->getLon(),'',5);
    }

    public function testConstructor() {
        $object=new LatLon();
        $this->assertEquals(0.0,$object->getLat());
        $this->assertEquals(0.0,$object->getLon());
        $this->assertEquals(6371.0,$object->getRadius());
        $object=new LatLon(4.23);
        $this->assertEquals(4.23,$object->getLat());
        $this->assertEquals(0.0,$object->getLon());
        $this->assertEquals(6371.0,$object->getRadius());
        $object=new LatLon(0.0,6.42);
        $this->assertEquals(0.0,$object->getLat());
        $this->assertEquals(6.42,$object->getLon());
        $this->assertEquals(6371.0,$object->getRadius());
        $object=new LatLon(0.4,6.42,5439.23);
        $this->assertEquals(0.4,$object->getLat());
        $this->assertEquals(6.42,$object->getLon());
        $this->assertEquals(5439.23,$object->getRadius());
    }
}
