<?php
namespace Bairwell\Geocoder;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2012-07-17 at 00:54:43.
 */
class FixedPrecisionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @covers Bairwell\Geocoder\FixedPrecision::toPrecisionFixed
     * @covers Bairwell\Geocoder\FixedPrecision::padZero
     */
    public function testToPrecisionFixed()
    {
        $this->assertEquals( '5.340000',FixedPrecision::toPrecisionFixed(5.34, 6));
        $this->assertEquals('25.5938',FixedPrecision::toPrecisionFixed(25.593798256, 6));
        $this->assertNull(FixedPrecision::toPrecisionFixed('abc',3));
        $this->assertEquals('0.000',FixedPrecision::toPrecisionFixed(0,3));
        $this->assertEquals('0.2340',FixedPrecision::toPrecisionFixed(.234,4));
        $this->assertEquals('-23.5',FixedPrecision::toPrecisionfixed(-23.45,3));
        $this->assertEquals('-23.0450',FixedPrecision::toPrecisionfixed('-023.045',6));
        $this->assertEquals('-0.01000',FixedPrecision::toPrecisionfixed('-.01',6));
        $this->assertEquals('1234.567000',FixedPrecision::toPrecisionFixed(1234.567,10));
    }
}
