<?php
namespace Bairwell\Geocoder;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2012-07-14 at 16:45:37.
 */
class CoordtransformTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Coordtransform
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new Coordtransform;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * Test converting to and from the GB National Grid system
     * @covers Bairwell\Geocoder\Coordtransform::convert
     * @covers Bairwell\Geocoder\Coordtransform::convertEllipsoid
     * @covers Bairwell\Geocoder\Coordtransform::__construct
     */
    public function testConvertGBNationalGrid()
    {
        $result=$this->object->convert(Coordtransform::GB_NATIONAL_GRID,TRUE,new LatLon(51.141941,0.842149));
        $this->assertEquals(51.14251,$result->getLat(),'latitude',5);
        $this->assertEquals(0.840449,$result->getLon(),'longitude',5);
        $result=$this->object->convert(Coordtransform::GB_NATIONAL_GRID,TRUE,new LatLon(2.6576,180.7179));
        $this->assertEquals(2.6628,$result->getLat(),'latitude',5);
        $this->assertEquals(-179.2807,$result->getLon(),'longitude',5);
        $result=$this->object->convert(Coordtransform::GB_NATIONAL_GRID,FALSE,new LatLon(51.14251,0.840449));
        $this->assertEquals(51.141941,$result->getLat(),'latitude',5);
        $this->assertEquals(0.842149,$result->getLon(),'longitude',5);
    }

    /**
     * Test converting to and from the Irish National Grid system
     * @covers Bairwell\Geocoder\Coordtransform::convert
     * @covers Bairwell\Geocoder\Coordtransform::convertEllipsoid
     * @covers Bairwell\Geocoder\Coordtransform::__construct
     */
    public function testConvertIrishNationalGrid()
    {
        $result=$this->object->convert(Coordtransform::IRISH_NATIONAL_GRID,TRUE,new LatLon(51.141941,0.842149));
        $this->assertEquals(51.14251,$result->getLat(),'latitude',5);
        $this->assertEquals(0.840449,$result->getLon(),'longitude',5);
        $result=$this->object->convert(Coordtransform::IRISH_NATIONAL_GRID,TRUE,new LatLon(2.6576,180.7179));
        $this->assertEquals(2.6628,$result->getLat(),'latitude',5);
        $this->assertEquals(-179.2807,$result->getLon(),'longitude',5);
        $result=$this->object->convert(Coordtransform::IRISH_NATIONAL_GRID,FALSE,new LatLon(51.14251,0.840449));
        $this->assertEquals(51.141941,$result->getLat(),'latitude',5);
        $this->assertEquals(0.842149,$result->getLon(),'longitude',5);
    }

    /**
     * Tests exception
     * @covers Bairwell\Geocoder\Coordtransform::convert
     */
    public function testConvertException() {
        $mess=NULL;
        try {
            $this->object->convert(9999,TRUE,new LatLon());
        } catch (\Exception $e) {
            $mess=$e->getMessage();
        }
        $this->assertEquals($mess,'Unrecognised grid system');
    }
}
