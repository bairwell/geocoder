<?php
/**
 * Coordtransform utility.
 *
 * @package Bairwell
 * @subpackage Geocoder
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2012 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\Geocoder;

/**
 * Coordinate transformations, lat/Long WGS-84 <=> OSGB36.
 *
 * Based on Javascript code written by and (c) Chris Veness 2002-2012
 * http://www.movable-type.co.uk/scripts/coordtransform.js
 * http://www.movable-type.co.uk/scripts/latlon-convert-coords.html
 */
class Coordtransform
{

    /**
     * Indicates we are transfomring to or from the UK National grid
     */
    const GB_NATIONAL_GRID = 1;

    /**
     * Indicates we are transforming to or from the Irish grid system
     */
    const IRISH_NATIONAL_GRID = 2;

    /**
     * Ellipse parameters
     * @var array
     */
    private $ellipse;

    /**
     * HElmert transform parameters from WGS84 to other datums
     * @var array
     */
    private $datumTransform;


    /**
     * Constructor
     */
    public function __construct()
    {
        /**
         * a=semi-major axis a (m)
         * b=semi-minor axis b (m)
         */
        $this->ellipse = array(
            'WGS84' => array('a' => 6378137, 'b' => 6356752.3142, 'f' => 1 / 298.257223563),
            'GRS80' => array('a' => 6378137, 'b' => 6356752.314140, 'f' => 1 / 298.257222101),
            'Airy1830' => array('a' => 6377563.396, 'b' => 6356256.910, 'f' => 1 / 299.3249646),
            'AiryModified' => array('a' => 6377340.189, 'b' => 6356034.448, 'f' => 1 / 299.32496),
            'Intl1924' => array('a' => 6378388.000, 'b' => 6356911.946, 'f' => 1 / 297.0)
        );
        $this->datumTransform = array(
            'toOSGB36' => array(
                'tx' => -446.448, 'ty' => 125.157, 'tz' => -542.060, // m
                'rx' => -0.1502, 'ry' => -0.2470, 'rz' => -0.8421, // sec
                's' => 20.4894), // ppm
            'toED50' => array(
                'tx' => 89.5, 'ty' => 93.8, 'tz' => 123.1, // m
                'rx' => 0.0, 'ry' => 0.0, 'rz' => 0.156, // sec
                's' => -1.2), // ppm
            'toIrl1975' => array(
                'tx' => -482.530, 'ty' => 130.596, 'tz' => -564.557, // m
                'rx' => -1.042, 'ry' => -0.214, 'rz' => -0.631, // sec
                's' => -8.150) // ppm
        );
        // ED50: og.decc.gov.uk/en/olgs/cms/pons_and_cop/pons/pon4/pon4.aspx
        // strictly, Ireland 1975 is from ETRF89: qv
        // www.osi.ie/OSI/media/OSI/Content/Publications/transformations_booklet.pdf
        // www.ordnancesurvey.co.uk/oswebsite/gps/information/coordinatesystemsinfo/guidecontents/guide6.html#6.5
    }

    /**
     * Converts from one grid system to/from WGS84
     * @param int $gridSystem The grid system
     * @param bool $toWgs84 TRUE = Convert TO WGS 84, FALSE= convert FROM WGS 84 to grid system
     * @param LatLon $latLon
     * @return LatLon
     * @throws \Exception if grid system is unrecognised
     */
    public function convert($gridSystem, $toWgs84, LatLon $latLon)
    {
        switch ($gridSystem) {
            case self::GB_NATIONAL_GRID:
                $ellipse = $this->ellipse['Airy1830'];
                $datum = $this->datumTransform['toOSGB36'];
                break;
            case self::IRISH_NATIONAL_GRID:
                $ellipse = $this->ellipse['AiryModified'];
                $datum = $this->datumTransform['toIrl1975'];
                break;
            default:
                throw new \Exception('Unrecognised grid system');
                break;
        }
        if (FALSE === $toWgs84) {
            return $this->convertEllipsoid($latLon, $this->ellipse['WGS84'], $datum, $ellipse);
        } else {
            $flippedDatum = array();
            foreach ($datum as $param => $value) {
                $flippedDatum[$param] = -$value;
            }
            return $this->convertEllipsoid($latLon, $ellipse, $flippedDatum, $this->ellipse['WGS84']);
        }

    }

    /**
     * Convert lat/lon from one ellipsoidal model to another
     *
     * q.v. Ordnance Survey 'A guide to coordinate systems in Great Britain' Section 6
     *      www.ordnancesurvey.co.uk/oswebsite/gps/docs/A_Guide_to_Coordinate_Systems_in_Great_Britain.pdf
     *
     * @private
     * @param LatLon   $point lat/lon in source reference frame
     * @param array $e1    target ellipse parameters
     * @param array $t     Helmert transform parameters
     * @param array $e2 target ellipse parameters
     * @return LatLon lat/lon in target reference frame
     */
    private function convertEllipsoid(LatLon $point, $e1, $t, $e2)
    {
        // -- 1: convert polar to cartesian coordinates (using ellipse 1)

        $lat = deg2rad($point->getLat());
        $lon = deg2rad($point->getLon());

        $a = $e1['a'];
        $b = $e1['b'];

        $sinPhi = sin($lat);
        $cosPhi = cos($lat);
        $sinLambda = sin($lon);
        $cosLambda = cos($lon);
        $H = 24.7; // for the moment

        $eSq = (($a * $a) - ($b * $b)) / ($a * $a);
        $nu = $a / sqrt(1 - $eSq * $sinPhi * $sinPhi);

        $x1 = ($nu + $H) * $cosPhi * $cosLambda;
        $y1 = ($nu + $H) * $cosPhi * $sinLambda;
        $z1 = ((1 - $eSq) * $nu + $H) * $sinPhi;

        // -- 2: apply helmert transform using appropriate params

        $tx = $t['tx'];
        $ty = $t['ty'];
        $tz = $t['tz'];
        $rx = deg2rad($t['rx'] / 3600); // normalise seconds to radians
        $ry = deg2rad($t['ry'] / 3600);
        $rz = deg2rad($t['rz'] / 3600);
        $s1 = $t['s'] / 1e6 + 1; // normalise ppm to (s+1)

        // apply transform
        $x2 = $tx + ($x1 * $s1) - ($y1 * $rz) + ($z1 * $ry);
        $y2 = $ty + ($x1 * $rz) + ($y1 * $s1) - ($z1 * $rx);
        $z2 = $tz - ($x1 * $ry) + ($y1 * $rx) + ($z1 * $s1);

        // -- 3: convert cartesian to polar coordinates (using ellipse 2)

        $a = $e2['a'];
        $b = $e2['b'];
        $precision = 4 / $a; // results accurate to around 4 metres

        $eSq = (($a * $a) - ($b * $b)) / ($a * $a);
        $p = sqrt($x2 * $x2 + $y2 * $y2);
        $phi = atan2($z2, $p * (1 - $eSq));
        $phiP = 2 * pi();
        while (abs($phi - $phiP) > $precision) {
            $nu = $a / sqrt(1 - $eSq * sin($phi) * sin($phi));
            $phiP = $phi;
            $phi = atan2($z2 + $eSq * $nu * sin($phi), $p);
        }
        $lambda = atan2($y2, $x2);
        $H = $p / cos($phi) - $nu;

        return new LatLon(rad2deg($phi), rad2deg($lambda), $H);
    }
}