<?php
/**
 * Grid base class utility
 *
 * @package Bairwell
 * @subpackage Geocoder
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2012 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\Geocoder\Grids;

/**
 * Grid base class.
 */
abstract class AbstractGrid
{

    /**
     * Used in grid references
     */
    const FLOOR = 1;
    const ROUND = 2;
    const CEIL = 3;

    /**
     * Easting in metres from OS false origin
     * @var float
     */
    private $easting;

    /**
     * Northing in metres from OS false origin
     * @var float
     */
    private $northing;

    /**
     * Major semi-axes
     * @var float
     */
    protected $a;

    /**
     * Minor semi-axes
     * @var float
     */
    protected $b;

    /**
     * Scale factor on central meridian
     * @var float
     */
    protected $F0;

    /**
     * True origin of grid latitude
     * @var float
     */
    protected $lat0;

    /**
     * True origin of grid longitude
     * @var float
     */
    protected $lon0;

    /**
     * Northing of true origin in metres
     * @var float
     */
    protected $N0;

    /**
     * Easting of true origin in metres
     * @var float
     */
    protected $E0;

    /**
     * Setup the appropriate grid details here
     * @abstract
     * @return void
     */
    abstract protected function setup();

    /**
     * Constructor
     *
     * @param mixed $param LatLon sets the grid from the LatLon, string sets it from grid reference, float=easting
     * @param float $northing Northing (only used if $param is float)
     */
    public function __construct($param = NULL, $northing = NULL)
    {
        $this->setup();
        if (($param instanceof \Bairwell\Geocoder\LatLon) && (NULL === $northing)) {
            $this->latLonToGrid($param);
        } else if ((TRUE === is_string($param)) && (NULL === $northing)) {
            $this->setFromGridRef($param);
        } else {
            if (NULL !== $param) {
                $this->setEasting($param);
            }
            if (NULL !== $northing) {
                $this->setNorthing($northing);
            }
        }
    }

    /**
     * Convert latitude/longitude grid reference easting/northing coordinate
     *
     * @param \Bairwell\Geocoder\LatLon $point latitude/longitude
     * @return AbstractGrid
     */
    public function latLonToGrid(\Bairwell\Geocoder\LatLon $point)
    {
        $lat = deg2rad($point->getLat());
        $lon = deg2rad($point->getLon());
        $lat0 = deg2rad($this->lat0);
        $lon0 = deg2rad($this->lon0);
        $e2 = 1 - ($this->b * $this->b) / ($this->a * $this->a); // eccentricity squared
        $n = ($this->a - $this->b) / ($this->a + $this->b);
        $n2 = $n * $n;
        $n3 = $n * $n * $n;

        $cosLat = cos($lat);
        $sinLat = sin($lat);
        $nu = $this->a * $this->F0 / sqrt(1 - $e2 * $sinLat * $sinLat); // transverse radius of curvature
        $rho = $this->a * $this->F0 * (1 - $e2) / pow(1 - $e2 * $sinLat * $sinLat, 1.5); // meridional radius of curvature
        $eta2 = $nu / $rho - 1;

        $Ma = (1 + $n + (5 / 4) * $n2 + (5 / 4) * $n3) * ($lat - $lat0);
        $Mb = (3 * $n + 3 * $n * $n + (21 / 8) * $n3) * sin($lat - $lat0) * cos($lat + $lat0);
        $Mc = ((15 / 8) * $n2 + (15 / 8) * $n3) * sin(2 * ($lat - $lat0)) * cos(2 * ($lat + $lat0));
        $Md = (35 / 24) * $n3 * sin(3 * ($lat - $lat0)) * cos(3 * ($lat + $lat0));
        $M = $this->b * $this->F0 * ($Ma - $Mb + $Mc - $Md); // meridional arc

        $cos3lat = $cosLat * $cosLat * $cosLat;
        $cos5lat = $cos3lat * $cosLat * $cosLat;
        $tan2lat = tan($lat) * tan($lat);
        $tan4lat = $tan2lat * $tan2lat;

        $I = $M + $this->N0;
        $II = ($nu / 2) * $sinLat * $cosLat;
        $III = ($nu / 24) * $sinLat * $cos3lat * (5 - $tan2lat + 9 * $eta2);
        $IIIA = ($nu / 720) * $sinLat * $cos5lat * (61 - 58 * $tan2lat + $tan4lat);
        $IV = $nu * $cosLat;
        $V = ($nu / 6) * $cos3lat * ($nu / $rho - $tan2lat);
        $VI = ($nu / 120) * $cos5lat * (5 - 18 * $tan2lat + $tan4lat + 14 * $eta2 - 58 * $tan2lat * $eta2);

        $dLon = $lon - $lon0;
        $dLon2 = $dLon * $dLon;
        $dLon3 = $dLon2 * $dLon;
        $dLon4 = $dLon3 * $dLon;
        $dLon5 = $dLon4 * $dLon;
        $dLon6 = $dLon5 * $dLon;

        $N = $I + $II * $dLon2 + $III * $dLon4 + $IIIA * $dLon6;
        $E = $this->E0 + $IV * $dLon + $V * $dLon3 + $VI * $dLon5;
        $this->setEasting($E);
        $this->setNorthing($N);
        return $this;
    }

    /**
     * Convert grid reference easting/northing coordinate to latitude/longitude
     *
     * @param int $dp Number of decimal places for latitude/longitude. 5=< 0.01mm
     * @return \Bairwell\Geocoder\LatLon  latitude/longitude  of supplied grid reference
     */
    public function gridToLatLon($dp = 5)
    {
        $E = $this->getEasting();
        $N = $this->getNorthing();

        $precision = (float)('0.' . str_repeat(0, $dp - 1) . '1');
        $lat0 = $this->lat0 * pi() / 180;
        $lon0 = $this->lon0 * pi() / 180; // NatGrid true origin
        $e2 = 1 - ($this->b * $this->b) / ($this->a * $this->a); // eccentricity squared
        $n = ($this->a - $this->b) / ($this->a + $this->b);
        $n2 = $n * $n;
        $n3 = $n * $n * $n;

        $lat = $lat0;
        $M = 0;
        do {
            $lat = ($N - $this->N0 - $M) / ($this->a * $this->F0) + $lat;

            $Ma = (1 + $n + (5 / 4) * $n2 + (5 / 4) * $n3) * ($lat - $lat0);
            $Mb = (3 * $n + 3 * $n * $n + (21 / 8) * $n3) * sin($lat - $lat0) * cos($lat + $lat0);
            $Mc = ((15 / 8) * $n2 + (15 / 8) * $n3) * sin(2 * ($lat - $lat0)) * cos(2 * ($lat + $lat0));
            $Md = (35 / 24) * $n3 * sin(3 * ($lat - $lat0)) * cos(3 * ($lat + $lat0));
            $M = $this->b * $this->F0 * ($Ma - $Mb + $Mc - $Md); // meridional arc

        } while ($N - $this->N0 - $M >= $precision);

        $cosLat = cos($lat);
        $sinLat = sin($lat);
        $nu = $this->a * $this->F0 / sqrt(1 - $e2 * $sinLat * $sinLat); // transverse radius of curvature
        $rho = $this->a * $this->F0 * (1 - $e2) / pow(1 - $e2 * $sinLat * $sinLat, 1.5); // meridional radius of curvature
        $eta2 = $nu / $rho - 1;

        $tanLat = tan($lat);
        $tan2lat = $tanLat * $tanLat;
        $tan4lat = $tan2lat * $tan2lat;
        $tan6lat = $tan4lat * $tan2lat;
        $secLat = 1 / $cosLat;
        $nu3 = $nu * $nu * $nu;
        $nu5 = $nu3 * $nu * $nu;
        $nu7 = $nu5 * $nu * $nu;
        $VII = $tanLat / (2 * $rho * $nu);
        $VIII = $tanLat / (24 * $rho * $nu3) * (5 + 3 * $tan2lat + $eta2 - 9 * $tan2lat * $eta2);
        $IX = $tanLat / (720 * $rho * $nu5) * (61 + 90 * $tan2lat + 45 * $tan4lat);
        $X = $secLat / $nu;
        $XI = $secLat / (6 * $nu3) * ($nu / $rho + 2 * $tan2lat);
        $XII = $secLat / (120 * $nu5) * (5 + 28 * $tan2lat + 24 * $tan4lat);
        $XIIA = $secLat / (5040 * $nu7) * (61 + 662 * $tan2lat + 1320 * $tan4lat + 720 * $tan6lat);

        $dE = ($E - $this->E0);
        $dE2 = $dE * $dE;
        $dE3 = $dE2 * $dE;
        $dE4 = $dE2 * $dE2;
        $dE5 = $dE3 * $dE2;
        $dE6 = $dE4 * $dE2;
        $dE7 = $dE5 * $dE2;
        $lat = $lat - $VII * $dE2 + $VIII * $dE4 - $IX * $dE6;
        $lon = $lon0 + $X * $dE - $XI * $dE3 + $XII * $dE5 - $XIIA * $dE7;

        return new \Bairwell\Geocoder\LatLon(rad2deg($lat), rad2deg($lon));
    }

    /**
     * Takes an easting and northing from a grid and works out the numerical values to show (appropriatly
     * padded and rounded)
     *
     * @param float $e Easting
     * @param float $n Northing
     * @param int $rounding The rounding type
     * @param int $digits The number of digits wanted for each section
     * @return string the padded rounded
     * @throws \Exception If the rounding method is not recognised
     */
    protected function deGridAndRound($e, $n, $rounding, $digits)
    {
        // strip 100km-grid indices from easting & northing, and reduce precision
        switch ($rounding) {
            case self::FLOOR:
                $e = floor((fmod($e, 100000)) / pow(10, 5 - $digits / 2));
                $n = floor((fmod($n, 100000)) / pow(10, 5 - $digits / 2));
                break;
            case self::ROUND:
                $e = round((fmod($e, 100000)) / pow(10, 5 - $digits / 2));
                $n = round((fmod($n, 100000)) / pow(10, 5 - $digits / 2));
                break;
            case self::CEIL:
                $e = ceil((fmod($e, 100000)) / pow(10, 5 - $digits / 2));
                $n = ceil((fmod($n, 100000)) / pow(10, 5 - $digits / 2));
                break;
            default:
                throw new \Exception('Unrecognised rounding method');
        }

        return str_pad($e, $digits / 2, '0', STR_PAD_LEFT) . ' ' . str_pad($n, $digits / 2, '0', STR_PAD_LEFT);
    }

    /**
     * Converts standard grid reference ('SU387148') to fully numeric ref ([438700,114800]);
     *
     * @param string $gridref Standard format grid reference
     * @throws \Exception If grid reference is not supported for this class
     */
    abstract public function setFromGridRef($gridref);

    /**
     * Converts this numeric grid reference to standard OS grid reference
     * @param int $digits Precision of returned grid reference (6 digits = metres)
     * @param int $rounding The rounding system to use in eastings/northings
     * @throws \Exception If grid reference is not supported for this class
     * @return string This grid reference in standard format
     */
    abstract public function toGridRef($digits = 10, $rounding = self::FLOOR);

    /**
     * Sets easting in a fluent manner
     *
     * @param float $easting
     * @return $this
     */
    public function setEasting($easting)
    {
        $this->easting = $easting;
        return $this;
    }

    /**
     * Gets the easting
     * @return float
     */
    public function getEasting()
    {
        return $this->easting;
    }

    /**
     * Sets northing in a fluent manner
     * @param float $northing
     * @return $this
     */
    public function setNorthing($northing)
    {
        $this->northing = $northing;
        return $this;
    }

    /**
     * Gets the northing
     * @return float
     */
    public function getNorthing()
    {
        return $this->northing;
    }
}