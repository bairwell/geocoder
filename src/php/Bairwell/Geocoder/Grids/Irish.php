<?php
/**
 * Definitation of the Irish grid system.
 *
 * @package Bairwell
 * @subpackage Geocoder
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2012 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\Geocoder\Grids;

/**
 *  Irish grid co-ordinates system functions.
 *
 * Based on Javascript code written by and (c) Chris Veness 2002-2012
 * http://www.movable-type.co.uk/scripts/gridref.js
 * http://www.movable-type.co.uk/scripts/latlon-gridref.html
 */
class Irish extends AbstractGrid
{

    /**
     * Setup our grid details
     */
    protected function setup()
    {
        $this->a = 6377340.189;
        $this->b = 6356034.447; // Airy 1830 modified major & minor semi-axes
        $this->F0 = 1.000035; // IrishGrid scale factor on central meridian
        $this->lat0 = 53.50000;
        $this->lon0 = -8.00000; // IrishGrid true origin is 49ºN,2ºW
        $this->N0 = 250000;
        $this->E0 = 200000; // northing & easting of true origin, metres
    }


    /**
     *Converts standard grid reference ('M 739 647') to fully numeric ref ([73968 64719]);
     *   returned co-ordinates are in metres, centred on supplied grid square;
     *   no error-checking is done on gridref (bad input will give bad results or NaN)
     *
     * @param string $gridref Standard format OS grid reference
     */
    public function setFromGridRef($gridref)
    {
        // get numeric values of letter references, mapping A->0, B->1, C->2, etc:
        $l1 = ord(strtoupper(substr($gridref, 0, 1))) - ord('A');
        // shuffle down letters after 'I' since 'I' is not used in grid:
        if ($l1 > 7) {
            $l1--;
        }

        // convert grid letters into 100km-square indexes from false origin (grid square V):
        $e = $l1 % 5;
        $n = 4 - (floor($l1 / 5));
        // skip grid letters to get numeric part of ref, stripping any spaces:
        $gridref = substr(str_replace(' ', '', $gridref), 1);
        // append numeric part of references to grid index:
        $e .= substr($gridref, 0, strlen($gridref) / 2);
        $n .= substr($gridref, strlen($gridref) / 2);
        // normalise to 1m grid, rounding up to centre of grid square:
        switch (strlen($gridref)) {
            case 6:
                $e .= '50';
                $n .= '50';
                break;
            case 8:
                $e .= '5';
                $n .= '5';
                break;
            // 10-digit refs are already 1m
        }
        $this->setEasting($e);
        $this->setNorthing($n);
    }

    /**
     * Converts this numeric grid reference to standard OS grid reference
     * @param int $digits Precision of returned grid reference (6 digits = metres)
     * @param int $rounding The rounding system to use in eastings/northings
     * @throws \Exception If the rounding method is unrecognised
     * @return string This grid reference in standard format
     */
    public function toGridRef($digits = 10, $rounding = self::FLOOR)
    {
        $e = $this->getEasting();
        $n = $this->getNorthing();

        // get the 100km-grid indices
        $e100k = floor($e / 100000);
        $n100k = floor($n / 100000);

        if ($e100k < 0 || $e100k > 6 || $n100k < 0 || $n100k > 12) {
            return '';
        }

        // translate those into numeric equivalents of the grid letters
        $l2 = (19 - $n100k) * 5 % 25 + $e100k % 5;

        // compensate for skipped 'I' and build grid letter
        if ($l2 > 7) {
            $l2++;
        }
        $letPair = chr($l2 + ord('A'));

        $gridRef = $letPair . ' ' . $this->deGridAndRound($e, $n, $rounding, $digits);
        return $gridRef;
    }
}