<?php
/**
 * Geodesy representation conversion functions
 *
 * @package Bairwell
 * @subpackage Geocoder
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2012 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\Geocoder;

/**
 * Geodesy representation conversion functions.
 *
 * Based on Javascript code written by and (c) Chris Veness 2002-2012
 * http://www.movable-type.co.uk/scripts/geo.js
 * http://www.movable-type.co.uk/scripts/latlong.html
 */
class Geo
{

    /**
     * Parses string representing degrees/minutes/seconds into numeric degrees.
     *
     * This is very flexible on formats, allowing signed decimal degrees, or deg-min-sec optionally
     * suffixed by compass direction (NSEW). A variety of separators are accepted (eg 3º 37' 09"W)
     * or fixed-width format without separators (eg 0033709W). Seconds and minutes may be omitted.
     * (Note minimal validation is done).
     *
     * @param   string|float|int $dmsStr Degrees or deg/min/sec in variety of formats
     * @return float|NULL Degrees as decimal number
     * @throws  \Exception dmsStr is not a string
     */
    public function parse($dmsStr)
    {
        if (FALSE === is_string($dmsStr) && FALSE === is_numeric($dmsStr)) {
            throw new \Exception('parse must only be sent string or numeric values');
        }
        // check for signed decimal degrees without NSEW, if so return it directly
        if (TRUE === is_numeric($dmsStr) && TRUE === is_finite($dmsStr)) {
            return (float)$dmsStr;
        }

        // strip off any sign or compass dir'n & split out separate d/m/s
        $noSign = preg_replace('/^\-/', '', trim($dmsStr));
        $noNSWE = preg_replace('/[NSWE]/i', '', $noSign);
        $dms = preg_split('/[^0-9\.,]+/', $noNSWE);

        if ($dms[(count($dms) - 1)] === '') {
            // from trailing symbol
            array_splice($dms, (count($dms) - 1));
        }
        if ($dms[0] === '') {
            return NULL;
        }
        // and convert to decimal degrees...

        return $this->parseToDecimalDegrees($dms, $dmsStr);
    }

    /**
     * Parse an array and string to decimal degrees.
     *
     * @param array $dms The array of each part of the number
     * @param string $dmsStr The original string
     * @return float|null
     */
    private function parseToDecimalDegrees($dms, $dmsStr)
    {
        switch (count($dms)) {
            case 3:
                // interpret 3-part result as d/m/s
                $deg = ($dms[0] / 1 + $dms[1] / 60 + $dms[2] / 3600);
                break;
            case 2:
                // interpret 2-part result as d/m
                $deg = ($dms[0] / 1 + $dms[1] / 60);
                break;
            case 1:
                // just d (possibly decimal) or non-separated dddmmss
                $deg = $dms[0];
                // check for fixed-width unseparated format eg 0033709W
                break;
            default:
                return NULL;
        }
        if (1 === preg_match('/^\-|[WS]$/i', trim($dmsStr))) {
            // take '-', west and south as -ve
            $deg = -$deg;
        }
        return (float)$deg;
    }

    /**
     * Convert decimal degrees to deg/min/sec format.
     *
     *  - degree, prime, double-prime symbols are added, but sign is discarded, though no compass
     *    direction is added
     *
     * @param   float $deg Degrees
     * @param   string $format Return value as 'd', 'dm', 'dms'
     * @param   int $dp No of decimal places to use - default 0 for dms, 2 for dm, 4 for d
     * @return string $deg formatted as deg/min/secs according to specified format
     * @throws \Exception If invalid format passed
     */
    private function toDegreesMinutesSeconds($deg, $format = 'dms', $dp = NULL)
    {
        $formatDps = array('d' => 4, 'dm' => 2, 'dms' => 0);
        if (FALSE === is_numeric($deg)) {
            // give up here if we can't make a number from deg
            return '';
        }

        // default values
        if (NULL === $format) {
            $format = 'dms';
        }
        if (NULL === $dp) {
            if (TRUE === isset($formatDps[$format])) {
                $dp = $formatDps[$format];
            } else {
                throw new \Exception('Invalid format');
            }
        }

        $deg = abs($deg); // (unsigned result ready for appending compass dir'n)
        switch ($format) {
            case 'd':
                $dms = $this->toDegreesMinutesSecondsHandleDegrees($dp, $deg);
                break;
            case 'dm':
                $dms = $this->toDegreesMinutesSecondsHandleDegreesMinutes($dp, $deg);
                break;
            case 'dms':
                $dms = $this->toDegreesMinutesSecondsHandleDegreesMinutesseconds($dp, $deg);
                break;
        }

        return $dms;
    }

    /**
     * Handle degree formatting for toDegreesMinutesSeconds.
     *
     * @param int $dp The number of decimal places
     * @param float $deg The decimal degrees
     * @return string An appropriate representation of the degrees and associated symbols
     */
    private function toDegreesMinutesSecondsHandleDegrees($dp, $deg)
    {
        $d = sprintf('%01.' . (int)$dp . 'f', $deg); // round degrees
        if ($d < 100) {
            // pad with leading zeros
            $d = '0' . $d;
        }
        if ($d < 10) {
            $d = '0' . $d;
        }
        return $d . mb_convert_encoding('&#x00b0;', 'UTF-8', 'HTML-ENTITIES'); // add º symbol
    }

    /**
     * Handle degree and minute formatting for toDegreesMinutesSeconds.
     *
     * @param int $dp The number of decimal places
     * @param float $deg The decimal degrees
     * @return string An appropriate representation of the degrees and minutes and associated symbols
     */
    private function toDegreesMinutesSecondsHandleDegreesMinutes($dp, $deg)
    {
        $min = sprintf('%01.' . (int)$dp . 'f', $deg * 60); // convert degrees to minutes & round
        $d = floor(($min / 60)); // get component deg/min
        $m = sprintf('%01.' . (int)$dp . 'f', fmod($min, 60)); // pad with trailing zeros
        if ($d < 100) {
            // pad with leading zeros
            $d = '0' . $d;
        }
        if ($d < 10) {
            $d = '0' . $d;
        }
        if ($m < 10) {
            $m = '0' . $m;
        }
        return $d . mb_convert_encoding('&#x00b0;', 'UTF-8', 'HTML-ENTITIES') .
            $m . mb_convert_encoding('&#x2032;', 'UTF-8', 'HTML-ENTITIES'); // add º, ' symbols
    }

    /**
     * Handle degree, minute and second formatting for toDegreesMinutesSeconds.
     *
     * @param int $dp The number of decimal places
     * @param float $deg The decimal degrees
     * @return string An appropriate representation of the degrees, minutes and seconds and associated symbols
     */
    private function toDegreesMinutesSecondsHandleDegreesMinutesSeconds($dp, $deg)
    {
        $sec = sprintf('%01.' . (int)$dp . 'f', $deg * 3600); // convert degrees to seconds & round
        $d = floor(($sec / 3600)); // get component deg/min/sec
        $m = (floor(($sec / 60)) % 60);
        $s = sprintf('%01.' . (int)$dp . 'f', fmod($sec, 60)); // pad with trailing zeros
        if ($d < 100) {
            // pad with leading zeros
            $d = '0' . $d;
        }
        if ($d < 10) {
            $d = '0' . $d;
        }
        if ($m < 10) {
            $m = '0' . $m;
        }
        if ($s < 10) {
            $s = '0' . $s;
        }
        return $d . mb_convert_encoding('&#x00b0;', 'UTF-8', 'HTML-ENTITIES') .
            $m . mb_convert_encoding('&#x2032;', 'UTF-8', 'HTML-ENTITIES') . $s .
            mb_convert_encoding('&#x2033;', 'UTF-8', 'HTML-ENTITIES'); // add º, ', " symbols
    }

    /**
     * Convert numeric degrees to deg/min/sec latitude (suffixed with N/S).
     *
     * @param   float $deg Degrees
     * @param   string $format Return value as 'd', 'dm', 'dms'
     * @param   int $dp No of decimal places to use - default 0 for dms, 2 for dm, 4 for d
     * @return string Deg/min/seconds
     */
    public function toLat($deg, $format = 'dms', $dp = 0)
    {
        $lat = $this->toDegreesMinutesSeconds($deg, $format, $dp);
        if ('' === $lat) {
            return '';
        } else {
            // knock off initial '0' for lat!
            if ($deg < 0) {
                return substr($lat, 1) . 'S';
            } else {
                return substr($lat, 1) . 'N';
            }
        }
    }

    /**
     * Convert numeric degrees to deg/min/sec longitude (suffixed with E/W).
     *
     * @param   float $deg Degrees
     * @param   string $format Return value as 'd', 'dm', 'dms'
     * @param   int $dp No of decimal places to use - default 0 for dms, 2 for dm, 4 for d
     * @return string Deg/min/seconds
     */
    public function toLon($deg, $format = 'dms', $dp = 0)
    {
        $lon = $this->toDegreesMinutesSeconds($deg, $format, $dp);
        if ('' === $lon) {
            return '';
        } else {
            if ($deg < 0) {
                return $lon . 'W';
            } else {
                return $lon . 'E';
            }
        }
    }

    /**
     * Convert numeric degrees to deg/min/sec as a bearing (0º..360º).
     *
     * @param   float $deg Degrees
     * @param   string $format Return value as 'd', 'dm', 'dms'
     * @param   int $dp No of decimal places to use - default 0 for dms, 2 for dm, 4 for d
     * @return string Deg/min/seconds
     */
    public function toBrng($deg, $format = 'dms', $dp = 0)
    {
        $deg = fmod($deg + 360, 360); // normalise -ve values to 180º..360º
        $brng = $this->toDegreesMinutesSeconds($deg, $format, $dp);
        return str_replace('360', '0', $brng); // just in case rounding took us up to 360º!
    }
}