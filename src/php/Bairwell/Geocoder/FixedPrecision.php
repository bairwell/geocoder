<?php
/**
 * Formats a number.
 *
 * @package Bairwell
 * @subpackage Geocoder
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2012 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\Geocoder;

/**
 * Formats the significant digits of a number, using only fixed-point notation (no exponential).
 */
class FixedPrecision
{

    /**
     * Formats the significant digits of a number, using only fixed-point notation (no exponential).
     *
     * @param float $number
     * @param int $precision Number of significant digits to appear in the returned string
     * @return NULL|string A string representation of number which contains precision significant digits
     */
    public static function toPrecisionFixed($number, $precision)
    {
        if (FALSE === is_numeric($number)) {
            return NULL;
        }
        $sign = '';
        if ($number < 0) {
            $numb = (-$number); // can't take log of -ve number
            $sign = '-';
        } else {
            $numb = $number;
        }

        if ($numb === 0) {
            // can't take log of zero, just format with precision zeros
            return self::padZero($precision);
        }

        $scale = ceil(log($numb) * M_LOG10E); // no of digits before decimal

        $n = (string)(round($numb * pow(10, ($precision - $scale))));

        if ($scale > 0) {
            // add trailing zeros & insert decimal as required
            $l = $scale - strlen($n);
            while (($l--) > 0) {
                $n = $n . '0';
            }
            if ($scale < strlen($n)) {
                $n = substr($n, 0, $scale) . '.' . substr($n, $scale);
            }
        } else {
            // prefix decimal and leading zeros if required
            while (($scale++) < 0) {
                $n = '0' . $n;
            }
            $n = '0.' . $n;
        }
        return $sign . $n;
    }

    /**
     * Pad a zero to a decimal precision
     *
     * @param int $precision The precision to be padded
     * @return string The padded string
     */
    private static function padZero($precision)
    {
        $n = '0.';
        while ($precision--) {
            $n .= '0';
        }
        return $n;
    }
}