These files are copies taken from http://www.movable-type.co.uk for reference purposes.

They are (c) 2006-2012 Chris Veness - scripts-geo@movable-type.co.uk
Donations to https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=3737803

"I offer these formulæ & scripts for free use and adaptation as my contribution to the open-source info-sphere from which I have received so much. You are welcome to re-use these scripts [under a simple attribution license, without any warranty express or implied] provided solely that you retain my copyright notice and a link to this page."

Licensed under http://creativecommons.org/licenses/by/3.0/
        You are free:

        to Share — to copy, distribute and transmit the work
        to Remix — to adapt the work
        to make commercial use of the work
        Under the following conditions:

        Attribution — You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
        With the understanding that:

        Waiver — Any of the above conditions can be waived if you get permission from the copyright holder.
        Public Domain — Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
        Other Rights — In no way are any of the following rights affected by the license:
        Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
        The author's moral rights;
        Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
        Notice — For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.