<?php
/**
 * Used to generate random coordinates from nearby.org.uk for comparison
 *
 * Set the following to your nearby.org.uk API key
 */
$nearbyAPIKey='';
$baseurl='http://www.nearby.org.uk/api/convert.php?key=%KEY%&p=%COORD%&in=%IN%&wants=%WANTS%&output=text';

$gb=TRUE;
for ($i=1;$i<20;$i++) {
    if (TRUE===$gb) {
        // from http://www.ordnancesurvey.co.uk/oswebsite/gps/information/coordinatesystemsinfo/guidetonationalgrid/page13.html
        $allowedLetters=Array(
                 'HP',
            'HT','HU',
            'HY','HZ',

            'NA','NB','NC','ND',
            'NF','NG','NH','NJ','NK',
            'NL','NM','NO',
                 'NR','NS','NT','NU',
                 'NW','NX','NY','NZ',

            'OV',

                       'SC','SD','SE',
                  'SG','SH','SJ','SK',
                  'SM','SN','SO','SP',
                  'SR','SS','ST','SU',
             'SV','SW','SX','SY','SZ',

            'TA',
            'TF','TG',
            'TL','TM',
            'TQ','TR',
            'TV'
        );
        $wants='all';
        $in='gr-osgb36';
        $letter=$allowedLetters[rand(0,count($allowedLetters)-1)];
        $n1=str_pad(rand(0,99999),5,'0',STR_PAD_LEFT);
        $n2=str_pad(rand(0,99999),5,'0',STR_PAD_LEFT);
        $coord=$letter.$n1.$n2;
    } else {
        // presume ireland
        $letters='ABCDEFGHJKLMNOPQRSTUVWXYZ';
        $wants='en-irish,ll-wgs84';
        $in='gr-irish';
        $letter=$letters[rand(0,strlen($letters)-1)];
        $n1=str_pad(rand(0,99999),5,'0',STR_PAD_LEFT);
        $n2=str_pad(rand(0,99999),5,'0',STR_PAD_LEFT);
        $coord=$letter.$n1.$n2;
    }
    $url=strtr($baseurl,array('%KEY%'=>$nearbyAPIKey,'%COORD%'=>urlencode($coord),'%IN%'=>urlencode($in),'%WANTS%'=>urlencode($wants)));
    $data=file_get_contents($url);
    $lines = explode("\n",$data); //becuase there can be multiple lines...
    print '$coords[]=array('.PHP_EOL;
    print '"grid"=>"'.$coord.'",'.PHP_EOL;
    foreach ($lines as $line) {
        $matches=array();
        if (1===preg_match('/^en,(irish|osgb36),(\-?\d{4,8}),(\-?\d{4,8})$/',$line,$matches)) {
            print '"easting"=>"'.$matches[2].'","northing"=>"'.$matches[3].'",'.PHP_EOL;
        }
        if (1===preg_match('/^ll,wgs84,([\d\.\-]{3,20}),([\d\.\-]{3,20})$/',$line,$matches)) {
            print '"latitude"=>"'.$matches[1].'","longitude"=>"'.$matches[2].'"'.PHP_EOL;
        }
    }
    print ');'.PHP_EOL;
}