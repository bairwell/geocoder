These are just general notes used when constructing this component.

Contents:
    |- movabletype/ - A copy of the relevant web pages and Javascript from http://www.movable-type.co.uk/
    |- generateTests.php - A script to generate random test data from nearby.org.uk (requires API key) which is used in the FunctionalTest.php
    |- ellipsoid_projections.html - A list of which ellipsoids and projections are in common use and the details used
    |- README.md - This file
    |- usefulSites.txt - A list of useful websites I found during the course of development