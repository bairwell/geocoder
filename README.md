Geocoder
========

**Geocoder** is a simple-to-use PHP component that allows you to simply and easily convert to and from:
    * UK OS National Grid references, eastings and northings and OSGB36/Airy1830 latitude and longitude
    * Irish National Grid references, eastings and northings and Irish grid/Airy1830 modified latitude and longitude
    * WGS84/GRS80 (i.e. "Sat Nav/GPS" standard) latitude and longitude

Along with:
    * Calculating the "great circle" distance between points using the haversine formula
    * Destination point given distance and bearing from start point
    * Intersection of two paths given start points and bearings
    * Rhumb line (or loxodrome) of two points
    * Converting between degrees minutes and seconds (dms) to and from decimal degrees

This component would not have been possible without the open source/freely available Javascripts written by
Chris Veness and which are available at http://www.movable-type.co.uk/scripts/latlong.html upon which this code is VERY
heavily based.

Additional information was taken from http://www.ordnancesurvey.co.uk/oswebsite/gps/docs/A_Guide_to_Coordinate_Systems_in_Great_Britain.pdf

Currently this code is 96.29% unit tested with 46 tests and 192 assertions.

Copyright
=========
This module was originally written by Richard Chiswell of Bairwell Ltd.
Whilst Bairwell Ltd holds the copyright on this work, we have licenced it under the MIT licence.

Bairwell Ltd: [http://www.bairwell.com](http://www.bairwell.com) / Twitter: [http://twitter.com/bairwell](http://twitter.com/bairwell)
Richard Chiswell: [http://blog.rac.me.uk](http://blog.rac.me.uk) / Twitter: [http://twitter.com/rchiswell](http://twitter.com/rchiswell)

Original Javascript source code (c) [Chris Veness](http://www.movable-type.co.uk/scripts/latlong.html)

System-Wide Installation
------------------------

ComponentName should be installed using the [PEAR Installer](http://pear.php.net). This installer is the PHP community's de-facto standard for installing PHP components.

    sudo pear channel-discover http://pear.bairwell.com
    sudo pear install --alldeps http://pear.bairwell.com/Bairwell_Geocoder

As A Dependency On Your Component
---------------------------------

If you are creating a component that relies on ComponentName, please make sure that you add ComponentName to your component's package.xml file:

```xml
<dependencies>
  <required>
    <package>
      <name>Bairwell_Geocoder</name>
      <channel>pear.bairwell.com</channel>
      <min>0.1.0</min>
      <max>0.9.0</max>
    </package>
  </required>
</dependencies>
```

Usage
-----

The best documentation for ComponentName are the unit tests, which are shipped in the package.  You will find them installed into your PEAR repository, which on Linux systems is normally /usr/share/php/test.

Development Environment
-----------------------

If you want to patch or enhance this component, you will need to create a suitable development environment. The easiest way to do that is to install phix4componentdev:

    # phix4componentdev
    sudo apt-get install php5-xdebug
    sudo apt-get install php5-imagick
    sudo pear channel-discover pear.phix-project.org
    sudo pear -D auto_discover=1 install -Ba phix/phix4componentdev

You can then clone the git repository:

    # ComponentName
    git clone https://bitbucket.org/bairwell/geocoder.git

Then, install a local copy of this component's dependencies to complete the development environment:

    # build vendor/ folder
    phing build-vendor

To make life easier for you, common tasks (such as running unit tests, generating code review analytics, and creating the PEAR package) have been automated using [phing](http://phing.info).  You'll find the automated steps inside the build.xml file that ships with the component.

Run the command 'phing' in the component's top-level folder to see the full list of available automated tasks.

Licence
=======
This work is licensed under the MIT License

Copyright (c) 2012 [Bairwell Ltd - http://www.bairwell.com](ttp://www.bairwell.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

ChangeLog
=========
0.1 : First release